var $q = require('q');
var fs = require("fs");
var jsonfile = require('jsonfile');
var excelbuilder = require('msexcel-builder');
var dbUtil = require("../config/dbUtil");
var ObjectId = require('mongodb').ObjectID;
var MongoClient = require('mongodb').MongoClient;
var moment = require('moment-timezone');
var async = require('async');
var evDateUtil = require('../utils/EVCDateUtils');
//var Excel = require('exceljs');

var months = {
	0: "Jan",
	1: "Feb",
	2: "Mar",
	3: "Apr",
	4: "May",
	5: "Jun",
	6: "Jul",
	7: "Aug",
	8: "Sep",
	9: "Oct",
	10: "Nov",
	11: "Dec"
};



exports.studentAttendanceMonthlyStats = function(reqObj) {
	var $attendancePromise = $q.defer();

	if (!(reqObj && reqObj.tenant && reqObj.year && reqObj.branchId && reqObj.subjectId)) {
		$attendancePromise.reject({
			status: "error",
			message: "Invalid requestObject"
		});
	}



	var startDate = reqObj.startDate;
	var endDate = reqObj.endDate;
	var currentDate = new Date();
	if (!startDate) {
		startDate = year + "/07/01";
	}
	if (!endDate) {
		endDate = (parseInt(year) + 1) + "/06/30";
	}

	var tableName = "T_" + reqObj.tenant + "_" + reqObj.year + "_" + reqObj.branchId + "_" + reqObj.subjectId + "_" + "AttendanceRecords";

	dbUtil.getConnection(function(db) {
		var attendanceCollection = db.collection(tableName);
		attendanceCollection.aggregate([{
				$match: {
					date: {
						$gte: startDate,
						$lte: endDate
					}
				}
			}, {
				$project: {
					status: 1,
					rollNo: 1,
					date: 1,
					stuName: 1,
					email: 1
				}
			}, {
				$group: {
					_id: {
						rollNo: "$rollNo",
						date: {
							$substr: ["$date", 0, 7]
						},
						stuName: "$stuName",
						email: "$email"
					},
					total: {
						$sum: 1
					},
					absent: {
						$sum: {
							$cond: {
								if: {
									"$eq": ["$status", "absent"]
								},
								then: 1,
								else: 0
							}
						}
					},
					present: {
						$sum: {
							$cond: {
								if: {
									"$eq": ["$status", "present"]
								},
								then: 1,
								else: 0
							}

						}
					}
				}
			}, {
				$group: {
					_id: {
						rollNo: "$_id.rollNo",
						stuName: "$_id.stuName",
						email: "$_id.email"
					},

					total: {
						$sum: "$total"
					},
					totalPresent: {
						$sum: "$present"
					},
					monthsStat: {
						"$push": {
							"month": "$_id.date",
							"total": "$total",
							"absent": "$absent",
							"present": "$present"
						}
					}
				}

			}],
			function(err, result) {
				if (err) {
					console.log("Error", err);
					$attendancePromise.reject({
						status: "error",
						"message": "something went wrong"
					});
				} else {
					var normalizedResult = normalizeStats(result, startDate, endDate);
					console.log("return result");
					$attendancePromise.resolve(result);
				}
			})
	});

	return $attendancePromise.promise;

};


exports.studentAttendanceCummStats = function(reqObj) {
	var $attendancePromise = $q.defer();
	if (!(reqObj && reqObj.tenant && reqObj.year && reqObj.branchId && reqObj.subjectId && reqObj.rollno && reqObj.startDate && reqObj.endDate)) {
		$attendancePromise.reject({
			status: "error",
			message: "Invalid requestObject"
		});
	}



	var startDate = reqObj.startDate;
	var endDate = reqObj.endDate;

	var tableName = "T_" + reqObj.tenant + "_" + reqObj.year + "_" + reqObj.branchId + "_" + reqObj.subjectId + "_" + "AttendanceRecords";

	dbUtil.getConnection(function(db) {
		var attendanceCollection = db.collection(tableName);
		attendanceCollection.aggregate([{
				$match: {
					rollNo: reqObj.rollno,
					date: {
						$gte: startDate,
						$lte: endDate
					}
				}
			}, {
				$group: {
					_id: "$rollNo",
					total: {
						$sum: 1
					},
					absent: {
						$sum: {
							$cond: {
								if: {
									"$eq": ["$status", "absent"]
								},
								then: 1,
								else: 0
							}
						}
					},
					present: {
						$sum: {
							$cond: {
								if: {
									"$eq": ["$status", "present"]
								},
								then: 1,
								else: 0
							}

						}
					}
				}
			}],
			function(err, result) {
				if (err) {
					console.log("Error", err);
					$attendancePromise.reject({
						status: "error",
						"message": "something went wrong"
					});
				} else {
					$attendancePromise.resolve(result);
				}
			})
	});

	return $attendancePromise.promise;

};

module.exports.generateExcel = function(reqObj, stream) {
	var fileWriterPromise = $q.defer();
	module.exports.studentAttendanceMonthlyStats(reqObj).then(function(data) {

			/*var filename = reqObj.tenant+reqObj.subjectId+reqObj.branchId+reqObj.startDate+reqObj.endDate+".xlsx";
			var options = {
				filename: './'+filename,
				useStyles: true,
				useSharedStrings: true
			};
			var workbook = new Excel.stream.xlsx.WorkbookWriter(options);
			var sheet = workbook.addWorksheet('MonthsStats');

*/
		},
		function(err) {

		});

}



var normalizeStats = function(toNormalize, startDate, endDate) {
	var data = toNormalize;
	var startDate = new Date(startDate);
	var endDate = new Date(endDate);
	var tempDate = new Date(startDate);
	var monthsArray = new Array();

	while (tempDate.getTime() <= endDate.getTime()) {
		var currMth = tempDate.getMonth() + 1;
		if (currMth < 10) {
			currMth = "0" + currMth;
		}
		var month = tempDate.getFullYear() + "/" + currMth;
		monthsArray.push(month);
		tempDate.setMonth(tempDate.getMonth() + 1)
	}

	console.log("MOnths ARray", monthsArray);


	for (var i = 0; i < data.length; i++) {
		var monthStatsLoc = [];
		var monthsStatsOrig = data[i].monthsStat;
		console.log("Stats Oring", monthsStatsOrig);
		monthsStatsOrig.sort((a, b) => {
			if (a.month > b.month)
				return 1;
			else if (a.month < b.month) {
				return -1;
			} else {
				return 0;
			}
		});
		var monthStatsOrigIdx = 0;

		for (var mnthIdx = 0; mnthIdx < monthsArray.length; mnthIdx++) {
			var monthL = monthsArray[mnthIdx];
			console.log("MOnth Array :", monthL, "at idx", mnthIdx);
			if (monthStatsOrigIdx < monthsStatsOrig.length) {
				var currMonthStat = monthsStatsOrig[monthStatsOrigIdx];
				if (currMonthStat && currMonthStat.month == monthL) {
					monthStatsOrigIdx++;
					monthStatsLoc.push(currMonthStat);
				} else {
					monthStatsLoc.push({
						month: monthL,
						total: 0,
						absent: 0,
						present: 0
					});
				}
			}
		}

		if (monthStatsLoc.length < monthsArray.length) {
			var monthSt = monthStatsLoc.length;
			while (monthSt < monthsArray.length) {
				var monthL = monthsArray[monthSt++];
				monthStatsLoc.push({
					month: monthL,
					total: 0,
					absent: 0,
					present: 0
				});
			}
		}
		data[i].monthsStat = monthStatsLoc;
	}
	return data;

}