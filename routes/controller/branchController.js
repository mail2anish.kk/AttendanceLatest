"use strict";
var dbUtil = require("../../config/dbUtil");
var ObjectId = require('mongodb').ObjectID;

exports.addBranch = function(req, res, next) {
    if (req.body && req.body.branchName && req.body.tenant && req.body.yearId) {

        var branchName = req.body.branchName
        var tenant = req.body.tenant
        var yearId = req.body.yearId

        try {
            var data = {
                'branchName': branchName,
                'branchId': 'branch'+Math.floor((Math.random() * 10) + 1) + "" + Math.floor((Math.random() * 10) + 1) + "" + Math.floor((Math.random() * 10) + 1) + "" + Math.floor((Math.random() * 10) + 1) + "" + Math.floor((Math.random() * 10) + 1) + Math.floor((Math.random() * 10) + 1) + "" + Math.floor((Math.random() * 10) + 1) + "" + Math.floor((Math.random() * 10) + 1) + "" + Math.floor((Math.random() * 10) + 1) + "" + Math.floor((Math.random() * 10) + 1),
                'yearId' : yearId
            };
            dbUtil.getConnection(function(db) {
                var tableName = "T_" + tenant + "_Branches";
                db.collection(tableName).find({
                    "branchName": branchName
                }).toArray(function(err, result) {
                    if(result.length > 0) {
                        res.json({
                                "error": "Branch already added"
                            });
                    }else{
                    db.collection(tableName).insertOne(data, function(err, result3) {
                        if (!err) {
                            res.json({
                                "success": "Branch added"
                            });
                        }
                    });
                }
                });
            });
        } catch (e) {
            res.json({
                "error": "Some error occurred. Please try again."
            });
        }
    } else {
        res.status(401).json({
            "error": "Parameters missing"
        });
    }
}


exports.getBranch = function(req, res, next) {
    if (req.body && req.body.tenant && req.body.yearId) {
        var tenant = req.body.tenant
        var yearId = req.body.yearId
        try {
            dbUtil.getConnection(function(db) {
                var tableName = "T_" + tenant + "_Branches";
                db.collection(tableName).find({
                    "yearId": yearId
                }).toArray(function(err, result) {
                    if(result.length > 0) {
                        res.json({'Branches':result});
                    }else{
                            res.json({
                                "error": "No branch added"
                            });
                    }
                });
            });
        } catch (e) {
            res.json({
                "error": "Some error occurred. Please try again."
            });
        }
    } else {
        res.status(401).json({
            "error": "Parameters missing"
        });
    }
}