'use strict';

// Declare app level module which depends on filters, and services

angular.module('myApp', [
  'ngRoute',
  'myApp.controllers',
  'myApp.filters',
  'myApp.services',
  'myApp.directives'
]).
config(['$routeProvider', '$httpProvider', '$locationProvider', function($routeProvider, $httpProvider, $locationProvider) {
  $routeProvider.when('/home', {
    templateUrl: '/kryptosattendance/dashboard/partials/home',
    controller: 'HomeCtrl'
  });
  $routeProvider.when('/login', {
    templateUrl: '/kryptosattendance/dashboard/partials/login',
    controller: 'LoginCtrl'
  });
  $routeProvider.when('/admin', {
    templateUrl: '/kryptosattendance/dashboard/partials/admin',
    controller: 'AdminCtrl'
  });
  $routeProvider.when('/details', {
    templateUrl: '/kryptosattendance/dashboard/partials/reportdetails',
    controller: 'DetailsCtrl'
  });
  $routeProvider.when('/cancelclasses', {
    templateUrl: '/kryptosattendance/dashboard/partials/cancelclasses',
    controller: 'CancelclassesCtrl'
  });
  $routeProvider.when('/monthlydetails', {
    templateUrl: '/kryptosattendance/dashboard/partials/reportdetailsmonthly',
    controller: 'MonthlyReportsCtrl',
    controllerAs: 'MonthlyReportsCtrl'
  });

  $routeProvider.when('/dashboard/:dbid', {
    templateUrl: '/partials/dashboard',
    controller: 'DashboardCtrl'
  });

  $routeProvider.when('/app/:appid/:pageid', {
    templateUrl: '/partials/app',
    controller: 'AppCtrl'
  });
  $routeProvider.otherwise({
    redirectTo: '/home'
  });
  $httpProvider.defaults.timeout = 5000;

}]);